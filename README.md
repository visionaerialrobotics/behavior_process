# Brief
This process is the father class of every behavior. It helps the developer to create new behaviors easily.

# Services
Every one of the following services are active within the namespace `/droneX/behavior_NAME/SERVICE_NAME`

- **~check_situation** ([aerostack_msgs/CheckSituation](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/CheckSituation.srv))  
Verifies if the behavior can be executed in that situation. For example, if the battery is low or if the drone is not in flying state.

- **~start** ([aerostack_msgs/StartBehavior](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/StartBehavior.srv))  
Calls the start function which also calls the ownStart function of the behavior.

- **~stop** ([aerostack_msgs/StopBehavior](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/srv/StopBehavior.srv))  
Calls the stop function which also calls the ownStop function of the behavior.

# Publish topics
- **behavior_event** ([aerostack_msgs/BehaviorEvent](https://bitbucket.org/joselusl/dronemsgsros/src/a6a289b1101d8a2de9799afe9c96cb540f524112/msg/BehaviorEvent.msg))  
Publishes the behavior and its termination every time the behavior ends. The terminations can be: GOAL_ACHIEVED, TIME_OUT, WRONG_PROGRESS, WRONG_PROGRESS or INTERRUPTED.

---
# Contributors
**Maintainer:** Alberto Camporredondo (alberto.camporredondo@gmail.com)  
**Author:** Alberto Camporredondo
