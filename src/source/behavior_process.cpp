/*!*******************************************************************************************
 *  \file       behavior_process.cpp
 *  \brief      BehaviorProcess implementation file.
 *  \details    This file implements the BehaviorProcess class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ********************************************************************************/

#include "../include/behavior_process.h"

/*Constructor*/
BehaviorProcess::BehaviorProcess() : RobotProcess()
{
  finish_condition_satisfied = false;
  timer_finished = false;
}

/*Destructor*/
BehaviorProcess::~BehaviorProcess()
{
  check_situation_srv.shutdown();
}

/*
------------------
Robot Process
------------------
*/
void BehaviorProcess::setUp()
{
  stop_server_srv = node_handler_robot_process.advertiseService(ros::this_node::getName() + "/stop",
                                                                &BehaviorProcess::stopBehaviorCallback, this);
  start_server_srv = node_handler_robot_process.advertiseService(ros::this_node::getName() + "/start",
                                                                 &BehaviorProcess::startBehaviorCallback, this);
  check_situation_srv = node_handler_robot_process.advertiseService(ros::this_node::getName() + "/check_situation",
                                                                    &BehaviorProcess::checkSituationCallback, this);

  setName(ros::this_node::getName().substr(ros::this_node::getName().find("_") + 1));

  ownSetUp();
  setState(STATE_READY_TO_START);
}

std::tuple<bool, std::string> BehaviorProcess::start()
{
  std::cout << "[BehaviorProcess | INFO]: Starting behavior..." << std::endl;
  setStarted(true);

  /*Activate basic topics*/
  behavior_event_pub =
      node_handle.advertise<aerostack_msgs::BehaviorEvent>("behavior_event", 1);  // TODO: do not hardcode

  /*Active countdown*/
  timer_finished = false;
  std::cout << "TIMEOUT: " << getTimeout() << std::endl;
  timer = node_handle.createTimer(ros::Duration(getTimeout()), &BehaviorProcess::timerCallback, this);

  /*Calling ownStart*/
  RobotProcess::start();

  if (!isStarted())
  {
    std::cout << "\t Error starting behavior" << std::endl;
    return std::make_tuple(false, "Error in ownStart() function. Behavior: [" + ros::this_node::getName() + "]");
  }

  std::cout << "\t Behavior started correctly" << std::endl;
  return std::make_tuple(true, "");
}

std::tuple<bool, std::string> BehaviorProcess::stop()
{
  std::cout << "[BehaviorProcess | INFO]: Stopping behaviors..." << std::endl;
  finish_condition_satisfied = false;

  RobotProcess::stop();

  /*shutdown basic topics*/
  return std::make_tuple(true, "");
}

void BehaviorProcess::run()
{
  RobotProcess::run();

  if (hasFinished())
  {
    std::cout << "[BehaviorProcess | INFO]: behavior {" << getName() << "} has finished." << std::endl;
    stop(); /*Stop behavior*/

    /*sending behavior_event*/
    aerostack_msgs::BehaviorEvent event_msg;
    std::string behavior_name = getName();
    std::transform(behavior_name.begin(), behavior_name.end(), behavior_name.begin(), ::toupper);
    event_msg.header.stamp = ros::Time::now();
    event_msg.name = behavior_name;
    event_msg.uid = getUID();
    event_msg.behavior_event_code = getFinishEvent();
    behavior_event_pub.publish(event_msg);
  }
}
/*
---------------
 Callbacks
---------------
*/
bool BehaviorProcess::startBehaviorCallback(aerostack_msgs::StartBehavior::Request& req,
                                            aerostack_msgs::StartBehavior::Response& resp)
{
  if (current_state == STATE_READY_TO_START)
  {
    setArguments(req.arguments);
    setTimeout(req.timeout);
    setUID(req.uid);

    bool ack;
    std::string error_message;
    std::tie(ack, error_message) = BehaviorProcess::start();

    resp.ack = ack;
    resp.error_message = error_message;
    return true;
  }
  ROS_WARN("Node %s received a start call when it was already running", ros::this_node::getName().c_str());
  resp.ack = false;
  resp.error_message = "Behavior [" + ros::this_node::getName() + "] is already active";
  return true;
}

bool BehaviorProcess::stopBehaviorCallback(aerostack_msgs::StopBehavior::Request& req,
                                           aerostack_msgs::StopBehavior::Response& resp)
{
  if (current_state == STATE_RUNNING)
  {
    std::tie(resp.ack, resp.error_message) = BehaviorProcess::stop();

    /*Sending behavior Event*/
    aerostack_msgs::BehaviorEvent event_msg;
    std::string behavior_name = getName();
    std::transform(behavior_name.begin(), behavior_name.end(), behavior_name.begin(), ::toupper);
    event_msg.header.stamp = ros::Time::now();
    event_msg.name = behavior_name;
    event_msg.behavior_event_code = aerostack_msgs::BehaviorEvent::INTERRUPTED;

    behavior_event_pub.publish(event_msg);

    return true;
  }
  ROS_WARN("Node %s received a stop call when it was already stopped", ros::this_node::getName().c_str());
  resp.ack = false;
  resp.error_message = "Behavior [" + ros::this_node::getName() + "] is not active";
  return true;
}

bool BehaviorProcess::checkSituationCallback(aerostack_msgs::CheckSituation::Request& req,
                                             aerostack_msgs::CheckSituation::Response& resp)
{
  std::tie(resp.situation_occurs, resp.error_message) = ownCheckActivationConditions();
  return true;
}

void BehaviorProcess::timerCallback(const ros::TimerEvent& timer_event)
{
  timer_finished = true;
}

/*
--------------
 Getters
--------------
*/
bool BehaviorProcess::isStarted()
{
  return started;
}

std::string BehaviorProcess::getName()
{
  return name;
}

std::string BehaviorProcess::getArguments()
{
  return arguments;
}

double BehaviorProcess::getUID()
{
  return uid;
}

int BehaviorProcess::getTimeout()
{
  return timeout;
}

bool BehaviorProcess::hasFinished()
{
  return finish_condition_satisfied;
}

aerostack_msgs::BehaviorEvent::_behavior_event_code_type BehaviorProcess::getFinishEvent()
{
  return finish_event;
}

bool BehaviorProcess::timerIsFinished()
{
  return timer_finished;
}

/*
----------------
 Setters
---------------
*/
void BehaviorProcess::setStarted(bool value)
{
  this->started = value;
}

void BehaviorProcess::setName(std::string name)
{
  this->name = name;
}

void BehaviorProcess::setArguments(std::string arguments)
{
  this->arguments = arguments;
}

void BehaviorProcess::setUID(double uid)
{
  this->uid = uid;
}

void BehaviorProcess::setTimeout(int timeout)
{
  this->timeout = timeout;
}

void BehaviorProcess::setFinishConditionSatisfied(bool value)
{
  finish_condition_satisfied = value;
}

void BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::_behavior_event_code_type event)
{
  finish_event = event;
}
