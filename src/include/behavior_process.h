/*!*********************************************************************************
 *  \file       behavior_process.h
 *  \brief      BehaviorProcess definition file.
 *  \details    This file contains the BehaviorProcess declaration.
 *              To obtain more information about it's definition consult
 *              the behavior_process.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ********************************************************************************/

#ifndef BEHAVIOR_PROCESS_H
#define BEHAVIOR_PROCESS_H

/*GNU/Linux*/
#include <string>
#include <tuple>
#include <map>

/*ROS*/
#include <ros/ros.h>

/*Aerostack Messages*/
#include <aerostack_msgs/CheckSituation.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include <aerostack_msgs/StartBehavior.h>
#include <aerostack_msgs/StopBehavior.h>

/*Aerostack Libraries*/
#include <robot_process.h>

/*Class definition*/
class BehaviorProcess : public RobotProcess
{
protected:
  BehaviorProcess();
  ~BehaviorProcess();

private: /*Variables*/
  std::string name;
  std::string arguments;
  double uid;
  int timeout;
  ros::Timer timer;
  bool timer_finished;
  bool started;

  /*ROS Variables*/
  ros::NodeHandle node_handle;
  bool finish_condition_satisfied;
  aerostack_msgs::BehaviorEvent::_behavior_event_code_type finish_event;

  ros::Publisher behavior_event_pub;
  ros::ServiceServer check_situation_srv;

protected:
  bool isStarted();
  std::string getName();
  std::string getArguments();
  double getUID();
  int getTimeout();
  bool timerIsFinished();
  bool hasFinished();
  aerostack_msgs::BehaviorEvent::_behavior_event_code_type getFinishEvent();

  void setStarted(bool value);
  void setName(std::string name);
  void setArguments(std::string arguments);
  void setUID(double uid);
  void setTimeout(int timeout);
  void setFinishConditionSatisfied(bool);
  void setFinishEvent(aerostack_msgs::BehaviorEvent::_behavior_event_code_type);

public: /*DroneProcess*/
  void setUp();
  std::tuple<bool, std::string> start();
  void run();
  std::tuple<bool, std::string> stop();

private:
  /*Callbacks*/
  bool startBehaviorCallback(aerostack_msgs::StartBehavior::Request&, aerostack_msgs::StartBehavior::Response&);
  bool stopBehaviorCallback(aerostack_msgs::StopBehavior::Request&, aerostack_msgs::StopBehavior::Response&);
  bool checkSituationCallback(aerostack_msgs::CheckSituation::Request&, aerostack_msgs::CheckSituation::Response&);
  void timerCallback(const ros::TimerEvent&);

protected: /*Virtual functions*/
  virtual void ownRun() = 0;
  virtual void ownSetUp() = 0;
  virtual void ownStart() = 0;
  virtual void ownStop() = 0;
  virtual std::tuple<bool, std::string> ownCheckActivationConditions() = 0;
};

#endif
